// 1. Create an object representation of yourself
// Should include: 
// - firstName
// - lastName
// - 'favorite food'
// - bestFriend (object with the same 3 properties as above)
const myself = {
 firstName: 'Fletcher', 
 lastName: 'Bonds', 
 favFood: 'Pie'
};
const bestFriend = {
 firstName: 'Travis',
 lastName: 'Barth',
 favFood: 'Pizza'
};


// 2. console.log best friend's firstName and your favorite food
console.log(`My best friend is ${bestFriend.firstName} and my favorite food is ${myself.favFood}`);

// 3. Create an array to represent this tic-tac-toe board
// -O-
// -XO
// X-X

let row1 = ['-','0','-']
let row2 = ['-','X','0']
let row3 = ['X','-','X']


// 4. After the array is created, 'O' claims the top right square.
// Update that value.
row1[2] = '0'

// 5. Log the grid to the console.
console.log(row1)
console.log(row2)
console.log(row3)

// 6. You are given an email as string myEmail, make sure it is in correct email format.
// Should be 1 or more characters, then @ sign, then 1 or more characters, then dot, then one or more characters - no whitespace
// i.e. foo@bar.baz
// Hints:
// - Use rubular to check a few emails: https://rubular.com/
// - Use regexp test method https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test
const testExp = /^\S+@\S+.\S{3}/;
console.log(testExp.test('foo@bar.com'));


// 7. You are given an assignmentDate as a string in the format "month/day/year"
// i.e. '1/21/2019' - but this could be any date.
// Convert this string to a Date
const assignmentDate = '1/21/2019';
const assignmentDateAsDate = new Date (Date.parse(assignmentDate))
console.log(assignmentDateAsDate)

// 8. Create a new Date instance to represent the dueDate.  
// This will be exactly 7 days after the assignment date.
const dayInMS = 86400000
const dueDate = new Date ((Date.parse(assignmentDate) + (7 * dayInMS)))
console.log(dueDate)

// 9. Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// I have provided a months array to help
const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

const day = dueDate.getDate()
const month = dueDate.getMonth()
const year = dueDate.getFullYear()
var mytag = `<time datetime=\"${year}-${month + 1}-${day}\">${months[month]} ${day}, ${year}</time>`

// 10. log this value using console.log
console.log(mytag)
